# Something Like The Plumber

*  This repository is originally sourced from
https://github.com/rmavis/something-like-the-plumber. For a nice
walk-through read more on
[something like Plan 9's plumber](http://richardmavis.info/something-like-the-plumber).
* **This file** overview the modifications done to fit my working box.

## Installations

* https://github.com/ilpianista/i3-sensible-browser
* https://github.com/Dushistov/sdcv


## Edits
```
s/sensible-browser/i3-sensible-browser/
s/dict/sdcv/
s/nautilus/dolphin/  # for file name and in-script command
```

## To try

An idea for the `dictionary` script, alternatively “abuse” rofi and dmenu?
Like:
```
echo "$(sdcv -0 -n new)" |rofi -dmenu -p "Result" -font "DejaVu Sans Mono 30" -bw 1 -no-custom
```

## Questions

* Why does `dolphin "$input";` fail with a path/string like
`~/code/something-like-the-plumber/scripts`? It prints:
  >  The file or folder `/home/nik/~/code/something-like-the-plumber/scripts` does
  not exist.